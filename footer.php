<?php 
	function footer($COLOR){
		echo "\n<!-- Footer -->";
		echo "\n<footer class=\"page-footer $COLOR\">";
		echo "\n\t<div class=\"container\">";
		echo "\n\t\t<div class=\"row\">";
		echo "\n\t\t\t<div class=\"col l9 s9\">";
		echo "\n\t\t\t\t<h5 class=\"white-text\">Contact Us</h5>";
		echo "\n\t\t\t\t<p class=\"grey-text text-lighten-4\">To get in contact with our administrators, please use the links provided.</p>";
		echo "\n\t\t\t</div>";
		echo "\n\t\t\t<div class=\"col l3 s3\">";
		echo "\n\t\t\t\t<h5 class=\"white-text\">Links</h5>";
		echo "\n\t\t\t\t<ul>";
		echo "\n\t\t\t\t\t<li><a class=\"white-text\" href=\"https://www.google.com/search?q=no\">Facebook</a></li>";
		echo "\n\t\t\t\t\t<li><a class=\"white-text\" href=\"https://www.google.com/search?q=no\">Twitter</a></li>";
		echo "\n\t\t\t\t\t<li><a class=\"white-text\" href=\"https://www.google.com/search?q=no\">Email</a></li>";
		//echo "\n\t\t\t\t\t<li><a class=\"white-text\" href=\"mailto:contact@pompeii.net\">Email</a></li>";
		echo "\n\t\t\t\t</ul>";
		echo "\n\t\t\t</div>";
		echo "\n\t\t</div>";
		echo "\n\t</div>";
		echo "\n\t<div class=\"footer-copyright\">";
		echo "\n\t\t<div class=\"container\">";
		echo "\n\t\t\tMade with <a class=\"brown-text text-lighten-3\" href=\"https://materializecss.com\">Materialize</a>";
		echo "\n\t\t</div>";
		echo "\n\t</div>";
		echo "\n</footer>";
	}
?>