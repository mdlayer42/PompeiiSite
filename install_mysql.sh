#!/bin/bash

# Various common scenes to search

sudo apt-get install -y mysql-server

mysql --version

sudo /etc/init.d/mysql start
sudo /etc/init.d/mysql restart

#sudo mysql_secure_installation --use-default

MYSQLLOGIN="mysql --host=localhost --user=dbviewer --password=password"

echo "CREATE USER 'pompeii'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';" | sudo mysql -u root 
echo "GRANT ALL ON * . * TO 'pompeii'@'localhost';" | sudo mysql -u root 
echo "FLUSH PRIVILEGES;" | sudo mysql -u root 

echo "DROP DATABASE PompeiiNet;" | sudo mysql -u root 
echo "CREATE DATABASE PompeiiNet;" | sudo mysql -u root 
echo -e "\n\nDB added, adding public user..."

# TODO: Refine public permissions
echo "CREATE USER 'public'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password'" | sudo mysql -u root 
echo "GRANT SELECT ON PompeiiNet . * TO 'public'@'localhost';" | sudo mysql -u root 
echo "FLUSH PRIVILEGES;" | sudo mysql -u root

MYSQLLOGIN="mysql --host=localhost --user=pompeii --password=password PompeiiNet"
echo -e "\n\nAdding tables"
echo "CREATE TABLE user_table(user_id INT NOT NULL AUTO_INCREMENT,join_date DATETIME NOT NULL,username_current VARCHAR(100) NOT NULL, PRIMARY KEY (user_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;" | `$MYSQLLOGIN`
echo "CREATE TABLE act_table(act_id INT NOT NULL,act_name VARCHAR(100) NOT NULL,PRIMARY KEY (act_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;" | `$MYSQLLOGIN`
echo "CREATE TABLE scene_table(scene_id INT NOT NULL,scene_name VARCHAR(200) NOT NULL,scene_slogan VARCHAR(200),act_id INT REFERENCES act_table(act_id), PRIMARY KEY (scene_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;" | `$MYSQLLOGIN`

echo "CREATE TABLE post_table(post_id INT NOT NULL AUTO_INCREMENT,scene_id INT NOT NULL REFERENCES scene_table(scene_id),user_id INT NOT NULL REFERENCES user_table(user_id),post_date DATETIME NOT NULL,post_content MEDIUMTEXT NOT NULL,edit_date DATETIME,edit_reason VARCHAR(500),editor VARCHAR(100), PRIMARY KEY (post_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8;" | `$MYSQLLOGIN`

echo -e "\n\nAdding standardized act IDs..."
echo "INSERT INTO act_table (act_id, act_name) VALUES ('1','ACT ONE');" | `$MYSQLLOGIN`
echo "INSERT INTO act_table (act_id, act_name) VALUES ('2','ACT TWO');" | `$MYSQLLOGIN`
echo "INSERT INTO act_table (act_id, act_name) VALUES ('3','ACT THREE');" | `$MYSQLLOGIN`
echo "INSERT INTO act_table (act_id, act_name) VALUES ('4','ACT FOUR');" | `$MYSQLLOGIN`
echo "INSERT INTO act_table (act_id, act_name) VALUES ('5','ACT FIVE');" | `$MYSQLLOGIN`

echo -e "\n\nAdding standardized scene IDs..."
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('1','SCENE I', 'Rome. A street.', '1');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('2','SCENE II', 'A public place.', '1');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('3','SCENE III', 'The same. A street.', '1');" | `$MYSQLLOGIN`

echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('4','SCENE I', \"Rome. BRUTUS's orchard.\", '2');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('5','SCENE II', \"CAESAR's house.\", '2');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('6','SCENE III', 'A street near the Capitol.', '2');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('7','SCENE IV', 'Another part of the same street, before the house of BRUTUS.', '2');" | `$MYSQLLOGIN`

echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('8','SCENE I', 'Rome. Before the Capitol; the Senate sitting above.', '3');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('9','SCENE II', 'The Forum.', '3');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('10','SCENE III', 'A street.', '3');" | `$MYSQLLOGIN`

echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('11','SCENE I', 'A house in Rome.', '4');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('12','SCENE II', \"Camp near Sardis. Before BRUTUS's tent.\", '4');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('13','SCENE III', \"Brutus's tent.\", '4');" | `$MYSQLLOGIN`

echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('14','SCENE I', 'The plains of Philippi.', '5');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('15','SCENE II', 'The same. The field of battle.', '5');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('16','SCENE III', 'Another part of the field.', '5');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('17','SCENE IV', 'Another part of the field.', '5');" | `$MYSQLLOGIN`
echo "INSERT INTO scene_table (scene_id, scene_name, scene_slogan, act_id) VALUES ('18','SCENE V', 'Another part of the field.', '5');" | `$MYSQLLOGIN`

# Insert script will fill user, post

# TODO: Indexing must be done AFTER all the posts are entered into the database
# CREATE FULLTEXT INDEX post_content_index (post_content) 