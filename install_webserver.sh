#!/bin/bash

# This script installs any necessary components and does necessary configuration to a server

DATABASENAME="PompeiiDB"
PHPVERSION="php7.2"

# TODO: Apache config and php config?
# TODO: Apache sites-available and sites-enabled?
sudo apt-get update
sudo apt-get -y install apache2 mysql-server vim nano python3 software-properties-common php-common
sudo apt-get -y install $PHPVERSION-mysql $PHPVERSION-cli libapache2-mod-$PHPVERSION $PHPVERSION $PHPVERSION-common

# This makes PHP work
echo "AddType application/x-httpd-php .php" | sudo tee -a  /etc/apache2/mime.conf
echo -e "<FilesMatch \\.php$>\n\tSetHandler application/x-httpd-php\n</FilesMatch>" | sudo tee -a /etc/apache2/apache2.conf

echo -e "\n\nCreating DB..."
chmod +x *.sh
./install_mysql.sh

echo -e "\n\nInserting database..."
sudo mysql -u root PompeiiNet < $DATABASENAME

echo -e "Indexing DB..."
MYSQLLOGIN="mysql --host=localhost --user=pompeii --password=password PompeiiNet"
echo "CREATE FULLTEXT INDEX post_content_index on post_table (post_content);" | `$MYSQLLOGIN`

echo -e "\n\nUpdating webserver..."
./update_webserver.sh

# TODO: mysql_secure_installation
# https://dev.mysql.com/doc/refman/5.7/en/mysql-secure-installation.html

# TODO: Add cron job
echo -e "# This performs a graceful restart every 5 minutes\n\n*/5 * * * * sudo /etc/init.d/apache2 graceful" | sudo tee -a /etc/cron.d/graceful_apache_restart
echo -e "\n\nCron Job added!"
