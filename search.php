<!DOCTYPE html>
<html lang="en">

<!-- Title -->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Search / Pompeii.net</title>

	<!-- CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
	<link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>

<body>
	<?php 
		require "footer.php";
		require "navbar.php";
		
		$COLOR = "red";
		
		navbar($COLOR, "search.php");
	?>
	
	<div id="index-banner" class="parallax-container row tooltipped" data-position="bottom" data-tooltip="'The Last Day of Pompeii' by Karl Bryullov">
		<div class="section no-pad-bot">
			<div class="container">
				<br><br>
				<h1 class="header center white-text">Search Pompeii's Records</h1>
				<div class="row center">
					<h5 class="header col s12 white-text">Scour the ancient records for your favorite words</h5>
				</div>
			</div>
		</div>
		
		<div class="parallax">
			<img src="images/the_last_day_of_pompeii.jpg" alt="'The Last Day of Pompeii' by Karl Bryullov">
		</div>
	</div>

	<div class="container">
		<div class="section">
			<!-- Icon Section -->
			<div class="row">
				<div class="col s12 m3">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons large">library_books</i></h2>
						<h5 class="center">Search All Pompeii.net Records</h5>
						<p class="light">You can search for a word in one of Pompeii.net's records</p>
					</div>
				</div>

				<div class="col s12 m9">
					<div class="icon-block">
						<h2 class="center brown-text"><i class="material-icons large">search</i></h2>
						<h5 class="center">Search All Pompeii.net Records</h5>
					</div>
				</div>
				
				<div class="col s12 m9">
					<form action="searchfor.php?" name="word_form" id="word_form" method="get">
						<div class="input-field col s9">
							<input placeholder="Caesar" id="search" name="search" type="text" class="validate">
							<label for="search">Search Term:</label>
						</div>
						
						<div class="row">
							<div class="col s12 m12">
								<h5>Records To Search:</h5>
								<p>
									<label>
										<input name='act' type="radio" value="1" class="with-gap" checked />
										<span>Act I</span>
									</label>
								</p>
								<p>
									<label>
										<input name='act' type="radio" value="2" class="with-gap" />
										<span>Act II</span>
									</label>
								</p>
								<p>
									<label>
										<input name='act' type="radio" value="3" class="with-gap" />
										<span>Act III</span>
									</label>
								</p>
								<p>
									<label>
										<input name='act' type="radio" value="1" class="with-gap" checked />
										<span>Act IV</span>
									</label>
								</p>
								<p>
									<label>
										<input name='act' type="radio" value="1" class="with-gap" checked />
										<span>Act V</span>
									</label>
								</p>
							</div>
						</div>
						
						<input type="submit" value="submit" value="word_form">
					</form>
				</div>
			</div>
		</div>
	</div>
	
	<?php
		footer($COLOR);
	?>
	
	<!-- Scripts -->
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script src="js/materialize.js"></script>
	<script src="js/init.js"></script>
	</body>
</html>
